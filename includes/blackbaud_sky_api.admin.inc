<?php

/**
 * Admin File for Blackbaud.
 */

use Drupal\blackbaud_sky_api\BlackbaudOauth;
use Drupal\blackbaud_sky_api\BlackbaudAPI;

/**
 * Blackbaud Settings Form.
 */
function blackbaud_sky_api_settings_form($form, &$form_state) {

  // Dev fieldset
  $form['dev'] = array(
    '#type' => 'fieldset',
    '#title' => t('Blackbaud API Development Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  // The Base Oauth URL
  $odesc = t('This should never change. This is here for future proofing. <a href="@bb" target="_blank">View the OAuth base url just in case</a>.', array('@bb' => 'https://apidocs.sky.blackbaud.com/docs/authorization/'));
  $form['dev']['blackbaud_sky_api_oauth_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Blackbaud SKY API Oath URL'),
    '#default_value' => variable_get('blackbaud_sky_api_oauth_url', BLACKBAUD_SKY_API_OAUTH_URL),
    '#description' => $odesc,
    '#required' => TRUE,
  );

  // The Base API URL
  $odesc = t('This should also never change. This is here for future proofing as well. <a href="@api" target="_blank">View the API base url just in case</a>.', array('@api' => 'https://apidocs.sky.blackbaud.com/docs/basics/'));
  $form['dev']['blackbaud_sky_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Blackbaud SKY API URL'),
    '#default_value' => variable_get('blackbaud_sky_api_url', BLACKBAUD_SKY_API_URL),
    '#description' => $odesc,
    '#required' => TRUE,
  );

  // The Developer Key
  $kdesc = t('Your Primary of Secondary Key Works Here. You can grab it from your <a href="@dev" target="_blank">Developer Profile</a>.', array('@dev' => 'https://developer.sky.blackbaud.com/developer/'));
  $form['dev']['blackbaud_sky_api_dev_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Blackbaud Developer Key'),
    '#default_value' => variable_get('blackbaud_sky_api_dev_key', ''),
    '#description' => $kdesc,
    '#required' => TRUE,
  );

  // Application fieldset
  $form['app'] = array(
    '#type' => 'fieldset',
    '#title' => t('Application Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  // Descriptive text
  $tdesc = t('You can obtain the application info from your <a href="@app" target="_blank">My Applications Page</a>.', array('@app' => 'https://developerapp.sky.blackbaud.com/applications'));
  $form['app']['title'] = array(
    '#type' => 'item',
    '#markup' => '<span><strong>' . $tdesc . '</strong></span>',
  );

  // The App ID.
  $form['app']['blackbaud_sky_api_application_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Application ID.'),
    '#default_value' => variable_get('blackbaud_sky_api_application_id', ''),
    '#required' => TRUE,
  );

  $form['app']['blackbaud_sky_api_application_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Application Secret.'),
    '#default_value' => variable_get('blackbaud_sky_api_application_secret', ''),
    '#required' => TRUE,
  );

  // The Redirect URI.
  $rdesc = t('The path without the domain and forward slash ie @path.', array('@path' => 'blackbaud/oauth'));
  $form['app']['blackbaud_sky_api_redirect_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect URI'),
    '#default_value' => variable_get('blackbaud_sky_api_redirect_uri', BLACKBAUD_SKY_API_REDIRECT_URI),
    '#description' => $rdesc,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Blackbaud Authorize Form.
 */
function blackbaud_sky_api_authorize_form($form, &$form_state) {
  // Check both methods to make sure we have auth.
  $token = variable_get('blackbaud_sky_api_access_token', '');

  // If we have a token.
  if (!empty($token)) {
    $bb = new BlackbaudAPI();
    $api = $bb->checkToken();
  }

  // Show the submit or not.
  if (empty($token) || !$api) {
    // We are not Authorized, instruct the people.
    $form['title'] = array(
      '#type' => 'item',
      '#markup' => '<center><h3>Blackbaud is not authorized on this site.  Click Authorize below and follow the prompts.<h3></center>',
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Authorize'),
      '#prefix' => '<center>',
      '#suffix' => '</center>',
    );
  }
  else {
    // We are good!
    $form['title'] = array(
      '#type' => 'item',
      '#markup' => '<center><h3>Blackbaud is authorized, you may carry on.<h3></center>',
    );
  }

  return $form;
}

/**
 * Blackbaud Authorize Form.
 */
function blackbaud_sky_api_authorize_form_validate($form, &$form_state) {
  // Instantiate the BlackBaud request and grab the code.
  $bb = new BlackbaudOauth();
  $bb->getCode();
}

/**
 * Blackbaud Oauth Redirect URI Callback.
 */
function blackbaud_sky_api_redirect_uri_callback() {
  // Instantiate the BlackBaud request and Authorize.
  $bb = new BlackbaudOauth();
  if (isset($_GET['code'])) {
    $bb->getAuthCode('init', $_GET['code']);
  }
}
