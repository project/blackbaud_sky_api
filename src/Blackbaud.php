<?php

namespace Drupal\blackbaud_sky_api;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;

/**
 * Class Blackbaud.
 *
 * @package Drupal\blackbaud_sky_api
 */
abstract class Blackbaud {

 /**
  * The Redirect URI.
  *
  * @var string
  */
  protected $redirect_uri;

 /**
  * The url we are checking.
  *
  * @var string
  */
  protected $url;

  /**
   * The Request Response.
   *
   * @var GuzzleHttp\Client
   */
  protected $request;

  /**
   * Constructor.
   */
  public function __construct() {
    // Sets up the redirect uri for later use.
    global $base_url;
    $base = $base_url;

    // Make sure we are on https.
    if (strpos($base, 'http:') !== FALSE) {
      $base = str_replace('http:', 'https:', $base);
    }

    // Set the full uri.
    $this->redirect_uri = $base . '/' . variable_get('blackbaud_sky_api_redirect_uri', '');
  }

  /**
   * Get the Oauth base URL we are checking.
   *
   * @return string
   *   The OAuth base URL.
   */
  protected function getOAuthBaseURL() {
    return variable_get('blackbaud_sky_api_oauth_url', BLACKBAUD_SKY_API_OAUTH_URL);
  }

  /**
   * Set the url we are checking.
   *
   * @param string $url
   *   The url string we are checking.
   */
  protected function setURL($url) {
    $this->url = $url;
  }

  /**
   * Get the url we are checking.
   *
   * @return string
   *   The url string we are checking.
   */
  protected function getURL() {
    return $this->url;
  }

  /**
   * Set the url we are checking.
   *
   * @param string $type
   *   The type of token we are setting.
   *
   * @param string $token
   *   The token we are setting.
   */
  protected function setToken($type, $token) {
    variable_set('blackbaud_sky_api_' . $type . '_token', $token);
  }

  /**
   * Get the url we are checking.
   *
   * @return string
   *   The type of token we are getting.
   */
  protected function getToken($type) {
    return variable_get('blackbaud_sky_api_' . $type . '_token', '');
  }

  /**
   * Get the Authorization code.
   *
   * @param string $type
   *   The type of Auth Code Request (init or refresh).
   *
   * @param string $code
   *   The $_GET param from the Oauth Callback page.
   */
  protected function getAuthCode($type, $code) {
    // Access Token Url
    $this->setURL($this->getOAuthBaseURL() . '/token');

    // Grab these for the auth header.
    $client_id = variable_get('blackbaud_sky_api_application_id', '');
    $client_secret = variable_get('blackbaud_sky_api_application_secret', '');

    // Set header for this post request.
    $header = array(
      'Content-type' => 'application/x-www-form-urlencoded',
      'Authorization' => 'Basic ' . base64_encode($client_id . ':' . $client_secret)
    );

    // Set body for the type of post request.
    switch ($type) {
      // Initial Request.
      case 'init':
        $body = array(
          'grant_type' => 'authorization_code',
          'code' => $code,
          'redirect_uri' => $this->redirect_uri,
        );
        break;

      // Refresh Request.
      case 'refresh':
        $body = array(
          'grant_type' => 'refresh_token',
          'refresh_token' => $code,
        );
        break;
    }

    // Set the options.
    $options = array(
      'headers' => $header,
      'body' => http_build_query($body),
    );

    // Post the Data.
    $this->requestResponse('POST', $options);

    // Decode and grab the response.
    $contents = json_decode($this->request->getBody()->getContents());

    // Set the refresh token for later use.
    $this->setToken('refresh', $contents->refresh_token);

    // Set the access token.
    $this->setToken('access', $contents->access_token);

    // What to do after we set this up.
    switch ($type) {
      // Initial Request.
      case 'init':
        // Go back to authorization page
        drupal_goto('admin/config/services/blackbaud/authorize');
        break;

      // Refresh Request.
      case 'refresh':
        return TRUE;
        break;
    }
  }

  /**
   * Gets the response of a page.
   *
   * @param string $type
   *   The type of request (ie GET, POST, etc).
   *
   * @param array $options
   *   Any options to pass in.
   */
  protected function requestResponse($type, $options = array()) {
    $client = new Client();

    // Set the options for the request.
    // @see http://docs.guzzlephp.org/en/latest/request-options.html
    $options += array(
      'http_errors' => FALSE,
      'timeout' => 3,
      'connect_timeout' => 3,
      'synchronous' => TRUE,
    );

    try {
      // Try the request.
      $response = $client->request($type, $this->getURL(), $options);

      // Check the Status code and return.
      switch ($response->getStatusCode()) {
        // All good, send back response.
        case '200':
          $this->request = $response;
          // Reset the quota variable.
          variable_set('blackbaud_sky_api_quota_reached', FALSE);
          break;

        // Need to reauth.
        case '401':
          $this->getAuthCode('refresh', $this->getToken('refresh'));
          break;

        // Quota reached.
        case '403':
          // Grab the message with the time.
          $message = json_decode($response->getBody()->getContents());
          $time = strstr($message->message, 'in ');
          $time = str_replace('in ', '', $time);

          // Convert this to seconds.
          $seconds = strtotime("1970-01-01 $time UTC");

          // Add this to NOW and what's left.
          $future = REQUEST_TIME + $seconds;

          // Set the readable date.
          $date = date('M j, Y H:i:s', $future);

          // Let the user know what's up.
          $message = 'You have reached your quota limit.  Your quota resets at ' . $date;
          watchdog('Blackbaud SKY API', $message);

          // Set variable and leave.
          variable_set('blackbaud_sky_api_quota_reached', TRUE);
          $this->request = NULL;
          break;

        // Something else is amiss.
        default:
          $message = 'The request to the Blackbaud API resulted in a ' . $response->getStatusCode() . ' Response.';
          watchdog('Blackbaud SKY API', $message);
          $this->request = NULL;
          break;
      }
    }
    catch (TransferException $e) {
      $this->request = NULL;
    }
  }
}
