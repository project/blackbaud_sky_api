<?php

namespace Drupal\blackbaud_sky_api;

use Drupal\blackbaud_sky_api\Blackbaud;

/**
 * Class BlackbaudOauth.
 *
 * @package Drupal\blackbaud_sky_api
 */
class BlackbaudOauth extends Blackbaud {

  /**
   * Gets the Code token via user interaction on a redirect.
   */
  public function getCode() {
    // Auth Url
    $this->setURL(parent::getOAuthBaseURL() . '/authorization');

    // Set the auth query params.
    $query = array(
      'query' => array(
        'client_id' => variable_get('blackbaud_sky_api_application_id', ''),
        'response_type' => 'code',
        'redirect_uri' => $this->redirect_uri,
      ),
    );

    // The Url used for auth.
    $url = url(parent::getURL(), $query);

    // Go to the url and return to the redirect URI.
    drupal_goto($url);
  }

  /**
   * @inheritdoc
   */
  public function getAuthCode($type, $code) {
    parent::getAuthCode($type, $code);
  }

}
