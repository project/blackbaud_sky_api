<?php

namespace Drupal\blackbaud_sky_api;

use Drupal\blackbaud_sky_api\Blackbaud;

/**
 * Class BlackbaudAPI.
 *
 * @package Drupal\blackbaud_sky_api
 */
class BlackbaudAPI extends Blackbaud {

  /**
   * Get the Developer Key.
   * The bb-api-subscription-key
   *
   * @return string
   *   The Developer Key.
   */
  protected function getDevKey() {
    return variable_get('blackbaud_sky_api_dev_key', '');
  }

  /**
   * Get the API base URL we are checking.
   *
   * @return string
   *   The OAuth base URL.
   */
  protected function getAPIBaseURL() {
    return variable_get('blackbaud_sky_api_url', BLACKBAUD_SKY_API_URL);
  }

  /**
   * Sets the typical headers needed for an api call.
   *
   * @return array
   *   the headers for the api call.
   */
  protected function setAPIHeaders() {
    // Grab the access token.
    $token = parent::getToken('access');

    // Return the set headers for an API call.
    return array(
      'headers' => array(
        'bb-api-subscription-key' => $this->getDevKey(),
        'Authorization' => 'Bearer ' . $token,
      ),
    );
  }

  /**
   * Mechanism to call the API.
   *
   * @param string $endpoint
   *   The endpoint url without the BASEapi url
   *
   * @param string $type
   *   The type of request (ie GET, POST, etc).
   *
   * @return null|object
   *   The API response or NULL.
   */
  public function callAPI($endpoint, $type = 'GET') {
    parent::setURL($this->getAPIBaseURL() . $endpoint);
    parent::requestResponse($type, $this->setAPIHeaders());

    // Exit if Empty Response.
    if (is_null($this->request)) {
      return NULL;
    }

    // Grab the Body and return as needed.
    $contents = json_decode($this->request->getBody()->getContents());
    return !empty($contents) ? $contents : NULL;
  }

  /**
   * Pings a simple api call to test if the token is valid.
   *
   * @return boolean
   *   If we are able to ping the API or not.
   */
  public function checkToken() {
    $check = $this->callAPI('/constituent/v1/addresstypes');
    return is_null($check) ? FALSE : TRUE;
  }
}
